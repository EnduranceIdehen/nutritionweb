﻿using Microsoft.AspNetCore.Mvc;

namespace NutritionDotNetCore.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
