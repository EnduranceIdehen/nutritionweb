namespace Core
{
    public class CalorieSet
    {
        public ServingMeasurement Measurement { get; set; }
        public Macronutrients Nutrients { get; set; }
    }
}