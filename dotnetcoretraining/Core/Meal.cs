﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core
{
    public class Meal
    {
        public string Name { get; set; }
        public List<CalorieSet> CalorieSet { get; set; }
    }

    public class Plan
    {
        public List<MealTime> Times { get; set; }
        public List<Meal> Meals { get; set; }

        public Plan(List<MealTime> times, List<Meal> meals)
        {
            Times = times;
            Meals = meals;
            FillMap(times, meals);
        }

        private void FillMap(List<MealTime> times, List<Meal> meals)
        {
            for (int i = 0; i < times.Count; i++)
            {
                var mealTime = times[i];
                var meal = meals[i];
                MealTimeMap.Add(mealTime, meal);
            }
        }

        public Dictionary<MealTime, Meal> MealTimeMap { get; set; }
    }

    public class MealTime
    {
        public DateTime When { get; set; }
    }

}