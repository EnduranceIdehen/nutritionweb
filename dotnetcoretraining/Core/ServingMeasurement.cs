namespace Core
{
    public class ServingMeasurement
    {
        public int ServingSize { get; set; }
        public string UnitIdentifier { get; set; }
    }
}