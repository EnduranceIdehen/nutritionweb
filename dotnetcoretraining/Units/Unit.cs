﻿using System;

namespace Units
{
    public interface IUnit
    {
        decimal Value { get; set; }
        string Name { get;}
        string Abbreviation { get;}
    }

    /// <summary>
    /// These exist in case I have some future ideas about handling differences in units
    /// </summary>
    public abstract class ImperialUnit : IUnit
    {
        public decimal Value { get; set; }
        public string Name { get; }
        public string Abbreviation { get; }
    }

    public abstract class MetricUnit : IUnit
    {
        public decimal Value { get; set; }
        public string Name { get; }
        public string Abbreviation { get; }
    }

    public class Ounce : IUnit
    {
        public decimal Value { get; set; }
        public string Name => "Ounce";
        public string Abbreviation => "oz";
    }
}