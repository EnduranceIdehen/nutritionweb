import datetime


class Food:
    def __init__(self, food_id: int, food_name: str, fat, carbohydrates, protein, serving_size, units: str):
        self.food_id = food_id
        self.food_name = food_name
        self.fat = fat
        self.carbohydrates = carbohydrates
        self.protein = protein
        self.serving_size = serving_size
        self.units = units

    @classmethod
    def createFood(cls, f: {}):
        return cls(food_id=f["food_id"], food_name=f["food_name"], fat=f["fat"],
                            carbohydrates=f["carbohydrates"],
                            protein=f["protein"], serving_size=f["serving_size"], units=f["units"])

    def __str__(self):
        pass


class Meal:
    def __init__(self, meal_id: int, meal_name: str, foods: [Food]):
        self.meal_id = meal_id
        self.meal_name = meal_name
        self.foods = foods

    def __str__(self):
        pass


class User:
    def __init__(self, user_id, user_name, email, password):
        self.user_id = user_id
        self.user_name = user_name
        self.email = email
        self.password = password

    def __str__(self):
        pass


class MealLog:
    def __init__(self, meallog_id: int, user: User, meal: Meal, meal_eaten_date: datetime):
        self.meal_log_id = meallog_id
        self.user = user
        self.meal = meal
        self.meal_eaten_date = meal_eaten_date

    def __str__(self):
        pass
