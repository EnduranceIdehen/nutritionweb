import datetime
import unittest

from repositories.sql.connect_sql import SQLConnect


class SQLConnectTest(unittest.TestCase):

    def test_connect(self):
        sql = SQLConnect()
        result = sql.query("SELECT * FROM User")
        print(str(result))

    def test_insert(self):
        sql = SQLConnect()
        sql.insert("schedule",
                   {'schedule_id': 0,
                    'user_id': 1, 'meal_id': 1, 'meal_time': datetime.datetime.now()})
        value = sql.get("schedule")
        print(value)
        sql.delete("Schedule", 0)
