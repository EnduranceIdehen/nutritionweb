from repositories.sql.repository import BaseRepository


class MealRepository(BaseRepository):
    table_name = "Meal"

    def get_meals_for_user(self, user_id):
        sql = "SELECT DISTINCT m.* FROM {} m INNER JOIN Ingredient i on i.meal_id = m.meal_id WHERE m.user_id = {}"\
            .format(self.table_name, user_id)
        return self._sql_manager.query(sql)
