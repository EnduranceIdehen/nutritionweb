from repositories.sql.repository import BaseRepository


class MealLogRepository(BaseRepository):
    table_name = "MealLog"

    def insert_into_meal_log(self, schedule_obj):
        return self._sql_manager.insert(self.table_name, schedule_obj)

    def delete_from_meal_log(self, schedule_id):
        return self._sql_manager.delete(self.table_name, schedule_id)

    def get_all_logs_for_user(self, user_id):
        return self._sql_manager.query("SELECT * FROM schedule s WHERE s.user_id = {0}".format(user_id))

    def get_meal_logs_for_range(self, user_id, range1, range2):
        sql = r"SELECT * FROM {0} " \
              "WHERE meal_eaten_date >= \"{1}\" AND meal_eaten_date <= \"{2}\" " \
              r"AND user_id = {3}".format(self.table_name, range1, range2, user_id)
        return self._sql_manager.query(sql)
