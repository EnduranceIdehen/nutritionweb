from repositories.sql.repository import BaseRepository


class ScheduleRepository(BaseRepository):
    table_name = "Schedule"

    def insert_into_schedule(self, schedule_obj):
        return self._sql_manager.insert(self.table_name, schedule_obj)

    def delete_from_schedule(self, schedule_id):
        return self._sql_manager.delete(self.table_name, schedule_id)

    def get_user_schedule(self, user_id):
        return self._sql_manager.query("SELECT * FROM schedule s WHERE s.user_id = {0}".format(user_id))

    def get_by_date(self, user_id, range1, range2):
        sql = r"SELECT * FROM schedule s " \
              "WHERE meal_time >= \"{0}\" AND meal_time <= \"{1}\" " \
              r"AND user_id = {2}".format(range1, range2, user_id)
        return self._sql_manager.query(sql)