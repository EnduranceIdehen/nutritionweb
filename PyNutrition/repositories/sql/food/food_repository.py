from repositories.sql.repository import BaseRepository


class FoodRepository(BaseRepository):
    table_name = "Food"

    def get_all_foods(self):
        return self.get_all()

    def get_food(self, food_id):
        return self.get(food_id)

    def get_foods_from_meal(self, meal_id):
        sql = "SELECT f.* FROM meal m " \
              "INNER JOIN ingredient i on m.meal_id = i.meal_id " \
              "INNER JOIN food f on f.food_id = i.food_id " \
              "WHERE m.meal_id = {0}".format(meal_id)
        return self._sql_manager.query(sql)

    def insert_food(self, name, fat, carbs, protein, serving_size, units):
        sql = "INSERT INTO {} (food_name, fat, carbohydrates, protein, serving_size, units) " \
              "VALUES (\"{}\",{},{},{},{},\"{}\")".format(self.table_name, name, fat, carbs, protein, serving_size, units)
        return self._sql_manager.query(sql)
