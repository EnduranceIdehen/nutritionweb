from injector import Module, inject

from repositories.sql.connect_sql import SQLConnect
from repositories.sql.repository import BaseRepository


class UserRepository(BaseRepository):

    table_name = "User"

    def get_all_users(self):
        return self._sql_manager.get_all()

    def get_by_user(self, user_name):
        return self._sql_manager.query("SELECT u.* FROM User u where user_name = \"{0}\"".format(user_name))


class UserRepositoryModule(Module):
    def configure(self, binder):
        binder.bind(SQLConnect)
