from injector import Module

from repositories.sql.food.food_repository import FoodRepository
from repositories.sql.food.meal_log_repository import MealLogRepository
from repositories.sql.food.meal_repository import MealRepository
from repositories.sql.food.schedule_repository import ScheduleRepository
from repositories.sql.users.user_repository import UserRepository


class UserRepositoryModule(Module):
    def configure(self, binder):
        binder.bind(UserRepository)


class FoodRepositoryModule(Module):
    def configure(self, binder):
        binder.bind(FoodRepository)


class MealRepositoryModule(Module):
    def configure(self, binder):
        binder.bind(MealRepository)


class MealLogRepositoryModule(Module):
    def configure(self, binder):
        binder.bind(MealLogRepository)


class ScheduleRepositoryModule(Module):
    def configure(self, binder):
        binder.bind(ScheduleRepository)


repository_modules = [UserRepositoryModule, FoodRepositoryModule, MealRepositoryModule,
                      ScheduleRepositoryModule, MealLogRepositoryModule]

