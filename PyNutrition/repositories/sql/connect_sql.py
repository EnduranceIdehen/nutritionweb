import sqlite3
import platform
path_to_localdb_macosx = "/Users/eidehen/Checkouts/nutritionweb/PyNutrition/repositories/sql/localdb"
path_to_localdb_windows = r"C:\Checkouts\NutritionWeb\PyNutrition\repositories\sql\localdb"

path_to_localdb = None
if platform.system() == 'Darwin':
    path_to_localdb = path_to_localdb_macosx
else:
    path_to_localdb = path_to_localdb_windows

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class SQLConnect:

    @staticmethod
    def _connect():
        return sqlite3.connect(path_to_localdb, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)

    def query(self, sql):
        con = self._connect()
        con.row_factory = dict_factory
        with con:
            cursor = con.cursor()
            cursor.execute(sql)
            return cursor.fetchall()

    def get(self, table_name):
        return self.query("SELECT * FROM " + table_name)

    def delete(self, table_name, table_id):

        return self.query("DELETE FROM " + table_name + " WHERE id = " + str(table_id))

    def insert(self, table_name, obj):
        sql = "INSERT INTO " + table_name + " ( "
        obj = self.convert_dict_to_obj(obj)
        keys = list()
        values = list()
        for key, value in obj.__dict__.items():
            keys.append(key)
            values.append("\"" + str(value) + "\"")

        for key in keys:
            sql += key + ","

        last_char = len(sql) - 1
        if sql[last_char] == ",":
            sql = sql[:-1]
        else:
            return -1

        sql += ") VALUES ("
        for value in values:
            sql += str(value) + ","
        sql = sql[:-1]
        sql += ")"

        return self.query(sql)

    def convert_dict_to_obj(self, py_dict):
        if isinstance(py_dict, dict):
            class pythonobj:
                pass
            obj = pythonobj()
            obj.__dict__ = py_dict
            return obj
        return py_dict

