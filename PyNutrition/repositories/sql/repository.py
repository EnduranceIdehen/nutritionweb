from injector import inject

from repositories.sql.connect_sql import SQLConnect


class BaseRepository:
    """
    Base Implementation of a SQL repository
    TODO: Replace as the base interface for any repository
    """
    table_name = None

    @inject(sql_connector=SQLConnect)
    def __init__(self, sql_connector):
        self._sql_manager = sql_connector

    def insert(self):
        pass

    def update(self):
        pass

    def get(self, primary_key):
        if isinstance(primary_key, int):
            return self._sql_manager.query("SELECT * FROM {0} where {0}_id = {1}".format(self.table_name, primary_key))
        else:
            return self._sql_manager.query("SELECT * FROM {0} where {0}_id = \"{1}\"".format(self.table_name, str(primary_key)))

    def get_all(self):
        return self._sql_manager.query("SELECT * FROM {0}".format(self.table_name))

