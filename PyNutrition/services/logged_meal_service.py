from injector import inject

from services.base_service import BaseService
from services.meal_log_service import MealLogService
from services.meal_service import MealService


class LoggedMealService(BaseService):
    @inject(meal_log_service=MealLogService, meal_service=MealService)
    def __init__(self, meal_log_service, meal_service):
        super().__init__(None)
        self._meal_log_service = meal_log_service
        self._meal_service = meal_service

    def get_meals_from_log(self, log_id=None, log_row=None):
        if log_row is not None:
            return self._meal_service.get_meal(log_row["meal_id"])
        elif log_id is not None:
            s = self._meal_log_service.get_logged_meal(log_id)
            return self._meal_service.get_meal(s)
        else:
            return None
