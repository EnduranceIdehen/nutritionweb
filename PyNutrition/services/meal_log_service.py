import datetime

from injector import inject

from repositories.sql.food.meal_log_repository import MealLogRepository
from services.base_service import BaseService


class MealLogService(BaseService):
    @inject(repo=MealLogRepository)
    def __init__(self, repo):
        super().__init__(repo)

    def log_meal(self, log_obj):
        return self._repo.insert_into_schedule(log_obj)

    def unlog_meal(self, log_id):
        return self._repo.delete_from_schedule(log_id)

    def get_logged_meal(self, log_id):
        self._repo.get(log_id)

    def get_all_logged_meals_for_day(self, user_id, month, day, year):
        first_value = datetime.datetime(year, month, day) \
            .replace(second=0, microsecond=0)
        second_value = first_value + datetime.timedelta(days=1)
        log_result = self._repo.get_meal_logs_for_range(user_id, first_value, second_value)
        return log_result
