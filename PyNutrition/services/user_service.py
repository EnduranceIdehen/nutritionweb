from core.models import User
from repositories.sql.users.user_repository import UserRepository
from services.base_service import BaseService
from injector import inject


class UserService(BaseService):
    """
    Captures User information for developer
    """

    @inject(repo=UserRepository)
    def __init__(self, repo):
        super().__init__(repo)

    def get(self, value_id):
        user = self._repo.get(value_id)
        return User(user["user_id"], user_name=user["user_name"], email=user["email"], password=user["password"])

    def get_by_user_name(self, user_name) -> []:
        # do some SQL injection tests here
        user_name = str(user_name).lower()
        return self._repo.get_by_user(user_name)

    def check_user_login(self, email, password):
        return True
