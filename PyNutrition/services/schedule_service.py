from injector import inject
import datetime
from repositories.sql.food.schedule_repository import ScheduleRepository
from services.base_service import BaseService


class ScheduleService(BaseService):
    @inject(repo=ScheduleRepository)
    def __init__(self, repo):
        super().__init__(repo)

    def schedule_meal(self, schedule_obj):
        return self._repo.insert_into_schedule(schedule_obj)

    def unschedule_meal(self, schedule_id):
        return self._repo.delete_from_schedule(schedule_id)

    def get(self, schedule_id):
        return self._repo.get(schedule_id)

    def get_user_schedule(self, user_id):
        return self._repo.get_user_schedule(user_id)

    def get_all_schedules_for_day(self, user_id, month, day, year):
        first_value = datetime.datetime(year, month, day)\
            .replace(second=0, microsecond=0)
        second_value = first_value + datetime.timedelta(days=1)
        schedule_result = self._repo.get_by_date(user_id, first_value, second_value)
        return schedule_result

    def get_all_meals_for_month(self, month):
        pass
