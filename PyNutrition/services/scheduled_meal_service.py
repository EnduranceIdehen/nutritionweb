from injector import inject

from services.base_service import BaseService
from services.meal_service import MealService
from services.schedule_service import ScheduleService


class ScheduledMealService(BaseService):
    @inject(schedule_service=ScheduleService, meal_service=MealService)
    def __init__(self, schedule_service, meal_service):
        super().__init__(None)
        self._schedule_service = schedule_service
        self._meal_service = meal_service

    def get_meals_from_schedule(self, schedule_id=None, schedule_row=None):
        if schedule_row is not None:
            return self._meal_service.get_meal(self._schedule_service.get(schedule_row["schedule_id"])[0]["meal_id"])
        elif schedule_id is not None:
            s = self._schedule_service.get(schedule_id)
            return self._meal_service.get_meal(s)
        else:
            return None

