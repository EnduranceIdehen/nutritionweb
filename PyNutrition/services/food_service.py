from injector import inject

from core.models import Food
from repositories.sql.food.food_repository import FoodRepository
from services.base_service import BaseService


class FoodService(BaseService):
    @inject(repo=FoodRepository)
    def __init__(self, repo):
        super().__init__(repo)

    def get_all_foods(self):
        return [Food.createFood(f) for f in self._repo.get_all_foods()]

    def get(self, food_id):
        f = self._repo.get_food(food_id)[0]
        return Food.createFood(f)

    def get_foods_from_meal(self, meal_id):
        return self._repo.get_foods_from_meal(meal_id)

    def create_food(self, name, fat, carbs, protein, serving_size, units):
        return self._repo.insert_food(name=name, fat=fat, carbs=carbs, protein=protein, serving_size=serving_size, units=units)
