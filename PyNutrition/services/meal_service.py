from injector import inject

from repositories.sql.food.meal_repository import MealRepository
from services.base_service import BaseService


class MealService(BaseService):
    @inject(repo=MealRepository)
    def __init__(self, repo):
        super().__init__(repo)

    def get_meal(self, meal_id):
        return self._repo.get(meal_id)

    def get_meals_for_user(self, user_id):
        """
        Get all meals for a user based on user primary key in db
        :param user_id:
        :return:
        """
        return self._repo.get_meals_for_user(user_id)

