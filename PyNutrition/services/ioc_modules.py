from injector import Module

from services.food_service import FoodService
from services.meal_service import MealService
from services.schedule_service import ScheduleService
from services.user_service import UserService
from services.meal_log_service import MealLogService


class UserServiceModule(Module):
    def configure(self, binder):
        binder.bind(UserService)


class FoodServiceModule(Module):
    def configure(self, binder):
        binder.bind(FoodService)


class MealServiceModule(Module):
    def configure(self, binder):
        binder.bind(MealService)


class MealLogServiceModule(Module):
    def configure(self, binder):
        binder.bind(MealLogService)


class ScheduleServiceModule(Module):
    def configure(self, binder):
        binder.bind(ScheduleService)

service_modules = [UserServiceModule, FoodServiceModule, MealServiceModule,
                   ScheduleServiceModule, MealLogService]

