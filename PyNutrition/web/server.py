from flask import Flask

from web.app.foods.food_manager import food_manager_blueprint
from web.app.home.index import index_blueprint
from web.app.meals.meal_manager import meal_manager_blueprint
from web.app.planning.log import log_blueprint
from web.app.user_management.login import login_blueprint

app = Flask(__name__)

app.register_blueprint(login_blueprint)
app.register_blueprint(meal_manager_blueprint)
app.register_blueprint(food_manager_blueprint)
app.register_blueprint(index_blueprint)
app.register_blueprint(log_blueprint)

if __name__ == '__main__':
    app.debug = True
    app.run()
