from flask import render_template, Blueprint, request
from services.user_service import UserService
from web.ioc.main_injector import injector

login_blueprint = Blueprint('login', __name__)
userService = injector.get(UserService)


@login_blueprint.route('/login')
def login_page():
    return render_template("user/login.html")


@login_blueprint.route('/login', methods=['POST'])
def login_post():
    text = request.form['username_text']
    users = userService.get_by_user_name(text)
    # If i don't find any users
    if len(users) <= 0:
        # bring "USER NOT FOUND" page
        return render_template("user/user_not_found.html")
    elif len(users) == 1:
        # else Welcome them to the website!
        return render_template("shared/../../templates/home/welcome.html", users=users[0])
