
from flask import render_template, Blueprint, request, redirect

from services.food_service import FoodService
from web.ioc.main_injector import injector

food_manager_blueprint = Blueprint('food_manager', __name__)

foodService = injector.get(FoodService)


@food_manager_blueprint.route('/food_manager')
def food_manager():
    foods = foodService.get_all_foods()
    return render_template("foods/food_manager.html", foods=foods, food_added=True)


@food_manager_blueprint.route('/food/<int:food_id>')
def find_a_food(food_id):
    food = foodService.get(food_id)
    return render_template("foods/food.html", food=food)


@food_manager_blueprint.route('/add_food')
def add_food_view():
    return render_template("foods/add_food.html")


@food_manager_blueprint.route('/add_food', methods=['POST'])
def post_add_food():
    name = request.form["food_name"]
    serving_size = request.form["serving_size_input"]
    units = request.form["select_units"]
    fat = request.form["fat_input"]
    carbs = request.form["carbs_input"]
    protein = request.form["protein_input"]
    foodService.create_food(name=name, units=units, serving_size=serving_size, fat=fat, carbs=carbs, protein=protein)
    return redirect('/food_manager')
