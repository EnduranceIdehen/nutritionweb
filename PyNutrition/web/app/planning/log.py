import datetime

from flask import render_template, Blueprint, redirect, url_for

from services.food_service import FoodService
from services.logged_meal_service import LoggedMealService
from services.meal_log_service import MealLogService
from services.meal_service import MealService
from services.schedule_service import ScheduleService
from services.scheduled_meal_service import ScheduledMealService
from services.user_service import UserService
from web.ioc.main_injector import injector

log_blueprint = Blueprint('log', __name__)
userService = injector.get(UserService)
foodService = injector.get(FoodService)
mealService = injector.get(MealService)
scheduleService = injector.get(ScheduleService)
scheduledMealService = injector.get(ScheduledMealService)

mealLogService = injector.get(MealLogService)
loggedMealService = injector.get(LoggedMealService)


@log_blueprint.route('/log')
def log_from_today():
    """
    Route that gets today's log for the user
    :return:
    """
    d = datetime.datetime.now().replace(second=0, microsecond=0)
    return redirect(url_for('log.log_main', month=d.strftime("%m"), day=d.strftime("%d"), year=d.strftime("%Y")))


@log_blueprint.route('/log/<int:month>/<int:day>/<int:year>', methods=['GET', 'POST'])
def log_main(month, day, year):
    """
    Displays the user's logged meals for a day specified
    :param month: 2-digit string for month
    :param day: 2-digit string for day
    :param year: 4-digit string for year
    :return:
    """
    list_of_logs = mealLogService.get_all_logged_meals_for_day(1, month, day, year)
    user = userService.get(1)[0]
    for log in list_of_logs:
        meals = loggedMealService.get_meals_from_log(log_row=log)
        for meal in meals:
            foods = foodService.get_foods_from_meal(meal["meal_id"])
            meal["foods"] = foods
        log["meals"] = meals
    return render_template("meals/../../templates/planning/log.html", user=user, logs=list_of_logs)

