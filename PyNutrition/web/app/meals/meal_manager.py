
from flask import render_template, Blueprint, request, redirect

from services.food_service import FoodService
from services.meal_service import MealService
from web.ioc.main_injector import injector

meal_manager_blueprint = Blueprint('meal_manager', __name__)

foodService = injector.get(FoodService)
mealService = injector.get(MealService)


@meal_manager_blueprint.route('/meal_manager')
def meal_manager_view():
    meals = mealService.get_meals_for_user(1)
    for meal in meals:
        meal["foods"] = foodService.get_foods_from_meal(meal["meal_id"])
    return render_template("meals/meal_manager.html", meals=meals)


@meal_manager_blueprint.route('/meal/<int:food_id>')
def find_a_meal(meal_id):
    pass
    # food = foodService.get(food_id)[0]
    # return render_template("meal_planning/meal.html", food=food)


@meal_manager_blueprint.route('/add_meal')
def add_meal_view():
    return render_template("meals/add_meal.html")


@meal_manager_blueprint.route('/add_meal', methods=['POST'])
def post_add_meal():
    name = request.form["food_name"]
    serving_size = request.form["serving_size_input"]
    units = request.form["select_units"]
    fat = request.form["fat_input"]
    carbs = request.form["carbs_input"]
    protein = request.form["protein_input"]
    foodService.create_food(name=name, units=units, serving_size=serving_size, fat=fat, carbs=carbs, protein=protein)
    return redirect('/meal_manager')
