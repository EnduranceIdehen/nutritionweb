from injector import Injector

from repositories.sql.ioc_modules import repository_modules
from services.ioc_modules import service_modules


injector = Injector(repository_modules + service_modules)
